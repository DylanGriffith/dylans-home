---
title: "Decomposing the GitLab backend database"
date: 2022-08-04T11:10:54+10:00
draft: false
---

Check out [my blog post on Decomposing the GitLab backend database](https://about.gitlab.com/blog/2022/08/04/path-to-decomposing-gitlab-database-part1/).
