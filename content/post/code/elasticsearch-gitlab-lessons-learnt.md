---
title: "Update: Elasticsearch lessons learnt for Advanced Global Search 2020-04-28"
date: 2020-04-28T11:10:54+10:00
draft: false
---

Check out [my blog post on lessons learnt deploying Advanced Global Search at
GitLab](https://about.gitlab.com/blog/2020/04/28/elasticsearch-update/)
